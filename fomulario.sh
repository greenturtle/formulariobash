#!/bin/bash
# Programa: formulario.sh
# Dependencias: nenhuma
# Autor: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Descrição: Script para cadastro e visualização de clientes, finalidade academica
# Parametros: nenhum
# Returno: 0
# Exemplo: chmod +x fakeData.sh; ./fakeData.sh

#DEFINE
menu_op=0
sc_op=0
client_list="clientList.txt"
invalid_message="Erro: opção inválida"
#FUNCTIONS
function continue {
	echo "Aperte qualquer tecla para continuar..."
	read
}
function checkClientList() {
	if [ ! -f $client_list ]; then
		touch clientList.txt
	fi
}
function showMenu() {
	clear
cat<< END
#-----------------------------------#
|GERENCIAR CLIENTES                 |
|-----------------------------------|
|1)Cadastrar cliente                |
|2)Visualizar clientes              |
|3)Sair                             |
#-----------------------------------#
END
	echo
	read -p "Escolha uma opção: " menu_op
}

function createClient() {
	clear
  echo "#-----------------------------------#"
	echo "|CADASTRAR CLIENTE                  |"
	echo "|-----------------------------------|"
	read -p "|Nome: " client_name
	read -p "|Sobrenome: " client_last_name
	read -p "|Idade: " client_age
	read -p "|Celular: " client_cellphone
	read -p "|E-mail: " client_email
	echo "Dados preenchidos, revise antes de salvar!"
	echo Deseja cadastrar este cliente?[S/n]
	read -n1 cc_op
	echo
	if [ $cc_op = "s" ] || [ $cc_op = "S" ]; then
		client_data=$(echo "Nome: $client_name Sobrenome: $client_last_name Idade: $client_age Celular: $client_cellphone E-mail: $client_email")
		echo $client_data >> $client_list
		echo "Salvo com sucesso!"
	else
		echo "Cadastro cancelado!"
	fi
	continue
}

function showClients() {
	clear
	sc_op=0
	until [ $sc_op -eq 5 ]
	do
cat<< END
#-----------------------------------#
|VISUALIZAR CLIENTES                |
|-----------------------------------|
|1)Todos os clientes                |
|2)Últimos 100 clientes             |
|3)100 primeiros clientes           |
|4)Ver o total de clientes          |
|5)Voltar                           |
|OBS:Aperte 'q' para sair do modo   |
|visualização                       |
#-----------------------------------#
END
	echo
	read -p "Escolha uma opção: " sc_op
	clear
	case $sc_op in
		1)
			cat -b $client_list | less;;
		2)
			tail -n100 $client_list | less;;
		3)
			head -n100 $client_list | less;;
		4)
			echo "#-----------------------------------#"
			echo "|Total de clientes: `cat -b $client_list | wc -l`"
			echo "#-----------------------------------#";;
		5)
			echo "Voltando...";;
		*)
			echo $invalid_message;;
	esac
	done
}

#SCRIPT
checkClientList
until [ $menu_op -eq 3 ]
do
	showMenu
	case $menu_op in
	1)
		createClient;;
	2)
		showClients;;
	3)
		echo "Saindo..."
		continue;;
	*)
		invalid_message;;
	esac
done
