#!/bin/bash
# Programa: fakeData.sh
# Dependencias: nenhuma
# Autor: Vinicius Richard Martiori (vrmsupport@protonmail.com)
# Descrição: Cria dados falsos para o formulario 
# Parametros: nenhum
# Returno: 0
# Exemplo: chmod +x fakeData.sh; ./fakeData.sh

qtdClients=500
client_list="clientList.txt"
client_data=""
client_name="FAKE"
client_last_name="da Silva"
client_age=19
client_cellphone="199875568"
client_email=fake
#SCRIPT
for (( i=0; i<qtdClients; i++))
do
	client_email=fake$i@gmail.com
	client_data=$(echo "Nome: $client_name$i Sobrenome: $client_last_name$i Idade: $client_age Celular: $client_cellphone$i E-mail: $client_email")
	echo $client_data	>> $client_list
done
